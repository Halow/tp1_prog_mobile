package com.example.uapv1502382.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class TP1 extends AppCompatActivity {

    ListView mListView;
    CountryList ctrL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tp1);

        mListView = (ListView) findViewById(R.id.listView);
        final ArrayAdapter<String> adapter =new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, ctrL.getNameArray());
        mListView.setAdapter(adapter);


        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(TP1.this, CountryActivity.class);
                intent.putExtra("countryName", parent.getItemAtPosition(position).toString());
                startActivity(intent);
            }
        });

    }


}
